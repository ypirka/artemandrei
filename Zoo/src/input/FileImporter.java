package input;


import animals.Animal;
import animals.Cat;
import animals.Wolf;
import jdk.nashorn.internal.runtime.regexp.joni.ast.ConsAltNode;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FileImporter {
    static LinkedList<Animal> animalsFromFile = new LinkedList<>();



    public static void importFromFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            System.out.println("This file is exists");
        } else {
            System.out.println("Not found");
        }

        try {
            ArrayList<String[]> arrAllStrings = new ArrayList<>();
            String tmp;
            BufferedReader br =
                    new BufferedReader(new FileReader(file));
            while ((tmp = br.readLine()) != null) {
                String[] temp = tmp.split(",");
                arrAllStrings.add(temp);
                switch (temp[0]){
                    case "Wolf":
                        animalsFromFile.add(new Wolf(temp[1], new Double(temp[2])) {
                        });
                        break;
                    case "Cat":
                        animalsFromFile.add(new Cat(temp[1], new Double(temp[2])) {
                        });
                        break;
                }


            }
            // в котором производите чтение файла,
            // определяйте количество животных в нем,
            // и преобразуйте их в объекты.
            // Используйте уже имеющиеся методы создания животных.


//            for (int i = 0; i < arrAllStrings.size(); i++) {
//                System.out.println((i + 1) + " " + arrAllStrings.get(i)[0] +
//                        " " + arrAllStrings.get(i)[1] + " " + arrAllStrings.get(i)[2]);
//            }

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static void animalToConsole(){
        for (int i=0;i<animalsFromFile.size();i++){
            System.out.println(animalsFromFile.get(i).toString());
        }
    }
}
